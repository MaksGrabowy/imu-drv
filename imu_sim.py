import csv
import socket
import struct

'''
    Available registers:
    - ACCEL_DATA - float 
    - GYRO_DATA - float : data in csv is in float so in case of those registers we output 32bit float values
    ACCEL_DATA  x 0x0b
                y 0x0c
                z 0x0d
    GYRO_DATA   x 0x0e
                y 0x0f
                z 0x10
    - GYRO_CONFIG0 - byte
    - ACCEL_CONFIG0 - byte
    - PWR_MGMT0 - byte
    - INT_STATUS_DRDY - byte
'''

START_CONDITION = b'\xff'
ACK = b'\x01'
NACK = b'\x02'
STOP_CONDITION = b'\xfe'

global cursor
global GYRO_CONFIG0
global ACCEL_CONFIG0
global PWR_MGMT0
global INT_STATUS_DRDY


global registers 
registers = {
    "31":["PWR_MGMT0",0],
    "32":["GYRO_CONFIG0",0],
    "33":["ACCEL_CONFIG0",0],
    "57":["INT_STATUS_DRDY",0]
}


cursor = 0 #current line fed to the simulator

csv_rows = []
with open('2023-01-16-15-33-09-imu.csv', 'r', newline='') as csvfile:
    imu_reader = csv.reader(csvfile, delimiter=',')
    for row in imu_reader:
        csv_rows.append(row)

csv_float = []
for i in range(1,len(csv_rows)):
    row = csv_rows[i]
    row_float = []
    for item in row:
        row_float.append(float(item))
    csv_float.append(row_float)

def get_starting_index(regis):
    j = 0
    for i in range(0,10+1,2):
        if(regis in range(11+i,12+i+1)):
            return j
        else:
            j+=1

#ax1, ax0, ay1, ay0, az1, az0, gx1, gx0, gy1, gy0, gz1, gz0

def imu_trans(client):
    global cursor
    global TEST_REG
    global registers
    data = client.recv(1)
    print("ADW-> " + str(hex(data[0])))

    print("<-ACK" + str(ACK))
    client.send(ACK)

    data = client.recv(1)
    print("RA -> " + str(hex(data[0]))) #here we have to set register we are reading from
    current_register = data[0]

    print("<-ACK" + str(ACK))
    client.send(ACK)

    data = client.recv(1)
    if data == START_CONDITION: #means we are reading
        print("S -> " + str(hex(data[0])))

        data = client.recv(1)
        print("ADR-> " + str(hex(data[0])))

        print("<-ACK" + str(ACK))
        client.send(ACK)

        if(current_register in range(11, 23)):
            accel_fsr_code = (registers["33"][1] >> 5)&0b11
            if(accel_fsr_code == 3):
                accel_FSR = 2.0
            elif(accel_fsr_code == 2):
                accel_FSR = 4.0
            elif(accel_fsr_code == 1):
                accel_FSR = 8.0
            elif(accel_fsr_code == 0):
                accel_FSR = 16.0
            print(str(accel_FSR))

            gyro_fsr_code = (registers["32"][1] >> 5)&0b11
            if(gyro_fsr_code == 3):
                gyro_FSR = 250.0
            elif(gyro_fsr_code == 2):
                gyro_FSR = 500.0
            elif(gyro_fsr_code == 1):
                gyro_FSR = 1000.0
            elif(gyro_fsr_code == 0):
                gyro_FSR = 2000.0       

            print(str(gyro_FSR))
            if(cursor < len(csv_float)):
                inner_cursor = get_starting_index(current_register)
                while True:
                    print(str(csv_float[cursor][inner_cursor]))
                    if(inner_cursor <= 2):
                        if(csv_float[cursor][inner_cursor] >= 0):
                            raw_value = int((csv_float[cursor][inner_cursor]*(2**14))/accel_FSR)
                        else:
                            raw_value = int((-csv_float[cursor][inner_cursor]*(2**14))/accel_FSR+(2**15))
                    else:
                        if(csv_float[cursor][inner_cursor] >= 0):
                            raw_value = int((csv_float[cursor][inner_cursor]*(2**14))/gyro_FSR)
                        else:
                            raw_value = int((-csv_float[cursor][inner_cursor]*(2**14))/gyro_FSR)+(2**15)
                    print(str(raw_value))
                    print("<-DAT " + str(((raw_value>>8)&255).to_bytes(1,'big')))
                    client.send(((raw_value>>8)&255).to_bytes(1,'big'))
                    inner_cursor += 1

                    data = client.recv(1)
                    if(data == NACK):
                        print("NCK-> " + str(hex(data[0])))
                        cursor += 1
                        break
                    print("ACK-> " + str(hex(data[0])))
                    print("<-DAT " + str((raw_value&255).to_bytes(1,'big')))
                    client.send((raw_value&255).to_bytes(1,'big'))

                    data = client.recv(1)
                    if(data == NACK):
                        print("NCK-> " + str(hex(data[0])))
                        cursor += 1
                        break
                    print("ACK-> " + str(hex(data[0])))
            else:
                print("No more data to send!")
                client.close()
                exit()
        else:
            index = 0
            while True:
                print("<-DAT" + str(registers[str(current_register+index)][1].to_bytes(1,'little')))
                # print(registers[str(current_register+index)][1].to_bytes(1,'little'))
                client.send(registers[str(current_register+index)][1].to_bytes(1,'little'))
                data = client.recv(1)
                if(data == NACK):
                    print("NCK-> " + str(hex(data[0])))
                    break
                print("ACK-> " + str(hex(data[0])))
                index += 1
                
        data = client.recv(1)
        print("STP-> " + str(hex(data[0])))

    else:  #means we are writing
        ret = []
        print("DAT-> " + str(hex(data[0])))
        ret.append(data[0])

        print("<-ACK" + str(ACK))
        client.send(ACK)

        while(True):
            data = client.recv(1)
            if data == STOP_CONDITION:
                print("STP-> " + str(hex(data[0])))
                break
            else:
                ret.append(data[0])
                print("DAT-> " + str(hex(data[0])))
                print("<-ACK" + str(ACK))
                client.send(ACK)
        # print(registers) 
        for i in range(len(ret)):
            # print("{0} to {1}".format(ret[i],current_register+i))
            registers[str(current_register+i)][1] = ret[i]
        # print(registers) 
        
def main():
    #ax, ay, az, gx, gy, gz
    print(len(csv_float))
    HOST = "127.0.0.1"
    PORT = 8084
    global registers

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((HOST, PORT))
    sock.listen()
    conn, addr = sock.accept()
    print(f"Connected by {addr}")
    registers["57"][1] = 1 #data ALWAYS ready
    while True:
        data = conn.recv(1)
        if(data == START_CONDITION):
            print("START TRANSMISSION-> " + str(hex(data[0])))
            imu_trans(conn)

if __name__ == "__main__":
    main()
    