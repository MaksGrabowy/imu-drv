# ICM-42670-P-drv
The purpouse of this project is to create a simulator and driver for i2c IMU device. 
Project requirements are:
- Driver written in C/C++
- Simulator written in Python
- Configuration of simulator must be possible without rebuilding project

## Tools used
- CMake
- Gcc
- C++
- Python
- Matlab
- VScode
- Ubuntu 23.10

## Driver
Driver provides methods for interacting with IMU:
- Writing - we can write data to specific registers in simulator memory. Available registers:
    - PWR_MGMT0 (0x1F) R/W
    - GYRO_CONFIG0 (0x20) R/W
    - ACCEL_CONFIG0 (0x21) R/W
- Reading - we can read data from registers. We can read from all registers we write to and additionally:
    - ACCEL_DATA_X1
    - ACCEL_DATA_X0
    - ACCEL_DATA_Y1
    - ...
    - GYRO_DATA_Z0
    - INT_STATUS_DRDY

Driver communicates with simulator using a TCP socket. It emulates i2c by treating each transission section as one byte. 
![Image showing read sequence](read_imu.png)
![Image showing write sequence](write_imu.png)

In this case we simulate sendind START, AD+W, RA, ACK, NACK .etc by sending just one byte at a time.

## Simulator
Simulator is a TCP server and accepts connection coming from driver. After that it listens for incomming data and based on that it determines its actions.

## Registers
Registers in IMU are direct outputs from 16-bit ADCs responsible for reading accel and gyro data. ADCs are outputting values in **two's complement** format. In simulator configuration registers are kept in dictionary structure.

## Reading
To read register we call:
```cpp
void drv::read_from_imu(int reg, int num, char *data_from_imu);
void drv::read_polling_from_imu(int reg, int num, char *data_from_imu);
```
Where we provide register number, number of bytes to read and pointer to array where method returns read bytes. Read_polling accomplishes the same thing but in polling mode so it checks INT_STATUS_DRDY register before reading. Reading ADC registers also return read and converted value straight into drv class private fields which are used to analyze free falling scenario.

## Writing 
Writing is very similiar to reading:
```cpp
void drv::write_to_imu(int reg, int num, char *data_src)
```
To write config we can directly use ```config_json()``` provided by drv.

## CSV 
Data is fed to the simulator from .csv file. Fields in this file are of 32-bit ```float``` type, but to simulate IMU correctly we have to convert them back to original ADC reading. The conversion is simple:
If value is >= 0
```math
Raw=\frac{value*2^{14}}{FSR}
```
If it is < 0 we have to set MSB to high so:
```math
Raw=\frac{value*2^{14}}{FSR} + 2^{15}
```
After that we can send two bytes back to driver. Driver reverses those operation and gets back original reading. 
**Note**: Reading may change based on config sent to simulator. With bigger resolution smaller values **will** be less accurate.
## config.json
[Config](../config.json) is a file containing all of the neccesary configuration information. Configuration file is read in driver using [C++ json library](https://github.com/nlohmann/json). In there you can specify simulator configuration and data acquisition configuration in driver:
```json
{ 
    "IMU" : {
        "_comment" : "we can't input hex values in JSON format (only available in JSON5) so we have to convert to decimal before inputting hex values",
        "PWR_MGMT0":{
            "ACCEL_LP_CLK_SEL" : 1,
            "IDLE" : 0,
            "GYRO_MODE" : 3,
            "ACCEL_MODE" : 3
        },
        "GYRO_CONFIG0":{
            "GYRO_UI_FS_SEL" : 3,
            "GYRO_ODR" : 10
        },
        "ACCEL_CONFIG0" :{
            "ACCEL_UI_FS_SEL" : 3,
            "ACCEL_ODR" : 10
        },
        "polling_mode" : false
    },
    "FreeFall" :{
        "threshold" : 0.2,
        "duration" : 0.1,
        "num_of_samples" : 3500
    }  

}
```
All fields are directly corresponding to registers listed in documentation of the module
## Freefall detection
For now we check freefall duration in main.cpp. To determine whether device is in freefall we check if data read from simulator matches this graph:
![ff graph](ff.png)

Driver provides method ```analyze_FF(thershold)``` to simply check if all acceleration readings are within given threshold.
## Unit tests
This project uses gtest for unit testing. So far only two (one) unit test were created for this project. Creator of this project never wrote unit tests before so he had to learn how to do it. First one:
```cpp
TEST(DrvTests, TestReturnValue)
{
    test_driver.init();
    const auto expected = 15;
    const auto actual = test_driver.ret_number(); //this is just test method that always returns 15
    ASSERT_EQ(expected, actual);
}
```
Driver provides test method that always return 15. This test checks if it works correctly.
Second test:
```cpp
TEST(DrvTests, TestConfig) //this test injects three random values into simulator config and then receives them back from simulator and checks if they are the same
{
    srand (time(NULL));
    test_driver.init();

    char original_data[3] = {0};
    original_data[0] = rand()*255;
    original_data[1] = rand()*255;
    original_data[2] = rand()*255;
    
    test_driver.config_inject(original_data);
    char config_read[3] = {0};
    test_driver.read_config(config_read);
    EXPECT_TRUE(0 == memcmp(&original_data, &config_read,3));
}
```
Purpouse of the second test is to determine if correct data is being written to and read from simulator. Firstly we create random array of 3 chars, then by using ```config_inject``` we send these bytes directly to simulator registers. After that we read from simulator and check if both arrays are the same.
**Note:** simulator has to be running during testing.
## Usage
To check usage of this project firstly run ```imu_sim.py``` and then is separate terminal run ```main.cpp```. Project is proved to work on Linux. Not sure about Windows. Additionally ```imu_helper.py``` and ```imu_helper.m``` can be used to plot data and check if it detects freefall correctly.
## Problems
Among many problems encountered on the way one is still unresolved. ```main.cpp ```crashes after ```return 0;``` statement. It is related to incorrect json object destruction, but cause of this problem remains unknown.