%% Data reading
clear all
close all
% Reading from CSV
data = readtable('2023-01-16-15-33-09-imu.csv', 'Delimiter', ',');
samples = 1:3500;
figure
plot(samples,data.ax, samples, data.ay, samples, data.az);

as = [data.ax data.ay data.az];
present_in_last = false;
timestamp = 0;
time_inc = 1.0/50.0;
fall_duration = 0.1;
timestamp_begin = 0;

thresh = 0.1;
for i = 1:3500
    if(abs(as(i,1))<thresh && abs(as(i,2))<thresh && abs(as(i,3))<thresh)
        if(~present_in_last)
            present_in_last = true;
            timestamp_begin = timestamp;
        else
            time_now = timestamp - timestamp_begin;
            if(time_now >= fall_duration)
                tex = sprintf("Falling at %.2f, sample: %d, ax: %.2f, ay: %.2f, az: %.2f ", ...
                    timestamp,i, as(i,1), as(i,2), as(i,3));
                disp(tex);
            end

        end
    else
        present_in_last = false;
    end
    timestamp = timestamp+time_inc;
end