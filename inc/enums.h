enum transmission_elements
{
    START_CONDITION = 0xff,
    ACK = 0x01,
    NACK = 0x02,
    STOP_CONDITION = 0xfe
};
