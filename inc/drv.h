#include "enums.h"
#include "structs.h"

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <json.hpp>

using json = nlohmann::json;
class drv{
    public:
        drv();
        ~drv();
        int init();
        void read_registers(int reg, int num, int* packet); //creates packets to send to imu simulator
        void write_registers(int reg, int num, char *data_src ,int* packet);
        void read_from_imu(int reg, int num, char *data_from_imu);
        void read_polling_from_imu(int reg, int num, char *data_from_imu);
        void write_to_imu(int reg, int num, char *data_src);
        void config_json(json config);
        void config_inject(char* config_bytes);
        void read_config(char* config_bytes);
        bool analyze_FF(float threshold);
        int ret_number();
        void exit();
    private:
        union{
            struct{
                float ax;
                float ay;
                float az;
                float gx;
                float gy;
                float gz;
            } readings;
            float readings_table[6];
        };
        gyro_params gyro_parameters;
        accel_params accel_parameters;

        int clientSocket = socket(AF_INET, SOCK_STREAM, 0);
        struct sockaddr_in serverAddress;
        int get_starting_index(int reg);

};
