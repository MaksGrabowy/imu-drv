include(FetchContent)

FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        release-1.11.0
)
FetchContent_MakeAvailable(googletest)
add_library(GTest::GTest INTERFACE IMPORTED)
target_link_libraries(GTest::GTest INTERFACE gtest_main)

add_executable(drv_tests drv_tests.cpp)

target_link_libraries(drv_tests
 PRIVATE
  GTest::GTest
  drv)

add_test(drv_gtests drv_tests)