#include <drv.h>
#include <gtest/gtest.h>
#include <stdlib.h>
#include <time.h>


#include <json.hpp>
#include <fstream>

using json = nlohmann::json;

drv test_driver;
TEST(DrvTests, TestReturnValue)
{
    test_driver.init();
    const auto expected = 15;
    const auto actual = test_driver.ret_number(); //this is just test method that always returns 15
    ASSERT_EQ(expected, actual);
}

TEST(DrvTests, TestConfig) //this test injects three random values into simulator config and then receives them back from simulator and checks if they are the same
{
    srand (time(NULL));
    test_driver.init();

    char original_data[3] = {0};
    original_data[0] = rand()*255;
    original_data[1] = rand()*255;
    original_data[2] = rand()*255;
    
    test_driver.config_inject(original_data);
    char config_read[3] = {0};
    test_driver.read_config(config_read);
    EXPECT_TRUE(0 == memcmp(&original_data, &config_read,3));
}