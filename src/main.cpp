
#include <iostream>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include "drv.h"
#include <cstring>
#include <unistd.h>

#include <json.hpp>
#include <fstream>

using json = nlohmann::json;

int main(int, char**){

    drv driver;
    driver.init();


    std::ifstream f("../config.json");
    json data = json::parse(f);
    f.close();

    driver.config_json(data);
    char config_read[3] = {0};
    // driver.read_from_imu(0x1f, 3, config_read); //we check if data was written corectly

    float thresh = data["FreeFall"]["threshold"];
    float freefall_time = data["FreeFall"]["duration"];
    int number_of_samples = data["FreeFall"]["num_of_samples"];
    bool polling_mode = data["IMU"]["polling_mode"];
    float timestamp = 0;
    float timestamp_begin = 0;
    bool present_in_last = false;
    float time_inc = 1.0/50.0;
    int data_count = 0;

    while(data_count <= number_of_samples){
        // firstly we create packet with bytes we will send to IMU
        // we want to read all of the values
        char read_from_imu[12] = {0};
        if(polling_mode){
            driver.read_polling_from_imu(0x0b, 6, read_from_imu);
        }else{
            driver.read_from_imu(0x0b, 12, read_from_imu);
        }

        // printf("Time: %f, sample: %d ax: %f, ay: %f, az: %f, gx: %f, gy: %f, gz: %f",timestamp,data_count,read_from_imu[0],read_from_imu[1],read_from_imu[2],read_from_imu[3],read_from_imu[4],read_from_imu[5]);
        if(driver.analyze_FF(thresh)){
            if(!present_in_last){
                present_in_last = true;
                timestamp_begin = timestamp;
            }else{
                if(timestamp-timestamp_begin >= freefall_time){
                    printf(" Free Falling: T= %f, Sample=%d \n",timestamp,data_count);
                }
            }
            
        }else{
            present_in_last = false;
        }
        timestamp = timestamp + time_inc;
        data_count++;

    }
    driver.exit();
    printf("All data read\n");
    //app crashes on return because of json library
    return 0;
}
