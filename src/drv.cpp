#include "drv.h"
#include <cstdint>
#include <cstring>
#include <iostream>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define IMU_ID 0x68 //typical from the documentation - b1101000 or b1101001
//#defune IMU_ID = 0x69

drv::drv(){

}

drv::~drv(){

}

int drv::init(){
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(8084);
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    connect(clientSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress));
    return 1;
}

void drv::exit(){
    close(clientSocket);
}

int drv::ret_number(){
    return 15;
}

void drv::read_registers(int reg, int num, int* packet){
    packet[0] = START_CONDITION;
    packet[1] = IMU_ID<<1; //shifting left gives us "0" at the oldest bit completing AD+W instruction
    packet[2] = 0xFF&reg;
    packet[3] = START_CONDITION;
    packet[4] = (IMU_ID<<1)|1; //shifting left and |1 gives us "1" at the oldest bit completing AD+R instruction
    for(int i = 0;i<num-1;i++){
        packet[5+i] = ACK;
    }
    packet[5+num-1] = NACK;
    packet[5+num-1+1] = STOP_CONDITION;
}

void drv::write_registers(int reg, int num, char *data_src ,int* packet){
    packet[0] = START_CONDITION;
    packet[1] = IMU_ID<<1; //shifting left gives us "0" at the oldest bit completing AD+W instruction
    packet[2] = 0xFF&reg;
    for(int i = 0;i<num;i++){
        packet[3+i] = data_src[i];
    }
    packet[3+num] = STOP_CONDITION;
}

void drv::config_json(json config){
    int accel_lp_clk_sel = config["IMU"]["PWR_MGMT0"]["ACCEL_LP_CLK_SEL"];
    int idle = config["IMU"]["PWR_MGMT0"]["IDLE"];
    int gyro_mode = config["IMU"]["PWR_MGMT0"]["GYRO_MODE"];
    int accel_mode = config["IMU"]["PWR_MGMT0"]["ACCEL_MODE"];

    char register_mgmt0 = 0xff&((accel_lp_clk_sel<<7)|(idle<<4)|(gyro_mode<<2)|(accel_mode));

    int gyro_ui_fs_sel = config["IMU"]["GYRO_CONFIG0"]["GYRO_UI_FS_SEL"];
    int gyro_odr = config["IMU"]["GYRO_CONFIG0"]["GYRO_ODR"];

    if(gyro_ui_fs_sel == 3)
        gyro_parameters.fsr = 250.0;
    else if(gyro_ui_fs_sel == 2)
        gyro_parameters.fsr = 500.0;
    else if(gyro_ui_fs_sel == 1)
        gyro_parameters.fsr = 1000.0;
    else if(gyro_ui_fs_sel == 0)
        gyro_parameters.fsr = 2000.0;

    char register_gyro_config0 = 0xff&((gyro_ui_fs_sel<<5)|(gyro_odr));

    int accel_ui_fs_sel = config["IMU"]["ACCEL_CONFIG0"]["ACCEL_UI_FS_SEL"];
    int accel_odr = config["IMU"]["ACCEL_CONFIG0"]["ACCEL_ODR"];

    if(accel_ui_fs_sel == 3)
        accel_parameters.fsr = 2.0;
    else if(accel_ui_fs_sel == 2)
        accel_parameters.fsr = 4.0;
    else if(accel_ui_fs_sel == 1)
        accel_parameters.fsr = 8.0;
    else if(accel_ui_fs_sel == 0)
        accel_parameters.fsr = 16.0;

    char register_accel_config0 = 0xff&((accel_ui_fs_sel<<5)|(accel_odr));

    char reg_to_write[3] =  {register_mgmt0, register_gyro_config0, register_accel_config0};
    write_to_imu(0x1f, 3, reg_to_write);
    // printf("%c",register_gyro_config0);
}
void drv::config_inject(char* injected){
    write_to_imu(0x1f,3,injected);
}

void drv::read_config(char* config_read){
    int read_packet[9] = {0};
    char rcv_buf;
    read_registers(0x1f, 3, read_packet);
    
    send(clientSocket, &read_packet[0], 1,0); //start

    send(clientSocket, &read_packet[1], 1,0); //ADW
    recv(clientSocket, &rcv_buf,1,0);
    // printf("%d/n",rcv_buf);

    send(clientSocket, &read_packet[2], 1,0); //RA
    recv(clientSocket, &rcv_buf,1,0);

    send(clientSocket, &read_packet[3], 1,0); //start
    send(clientSocket, &read_packet[4], 1,0); //ADR
    recv(clientSocket, &rcv_buf,1,0);
    int data_count = 0;
    do{  
        char data_rcv;
        recv(clientSocket, &data_rcv,1,0);
        // printf("%x\n",data_rcv);
        memcpy(&config_read[data_count++],&data_rcv,1);
        send(clientSocket, &read_packet[4+data_count], 1,0); //ACK/NACK
    }while(read_packet[4+data_count] != NACK);

    send(clientSocket, &read_packet[4+data_count+1], 1,0); //Stop
}

void drv::read_from_imu(int reg, int num, char *data_from_imu){
    // int read_packet[18] = {0};
    int read_packet[32] = {0};
    int count = 0;
    char rcv_buf;
    read_registers(reg, num, read_packet);    
    
    send(clientSocket, &read_packet[0], 1,0); //start

    send(clientSocket, &read_packet[1], 1,0); //ADW
    recv(clientSocket, &rcv_buf,1,0);
    // printf("%d/n",rcv_buf);

    send(clientSocket, &read_packet[2], 1,0); //RA
    recv(clientSocket, &rcv_buf,1,0);

    send(clientSocket, &read_packet[3], 1,0); //start
    send(clientSocket, &read_packet[4], 1,0); //ADR
    recv(clientSocket, &rcv_buf,1,0);
    int starting_index = get_starting_index(reg);
    int data_count = 0;
    do{  
        char data_rcv;
        recv(clientSocket, &data_rcv,1,0);
        // printf("%x\n",data_rcv);
        memcpy(&data_from_imu[starting_index + data_count++],&data_rcv,1);
        send(clientSocket, &read_packet[4+data_count], 1,0); //ACK/NACK
    }while(read_packet[4+data_count] != NACK);

    send(clientSocket, &read_packet[4+data_count+1], 1,0); //Stop

    int read_int[32] = {0};
    for(int i = 0;i<num;i++){
        read_int[starting_index + i] = (int)data_from_imu[starting_index + i]&0xff;
    }
    if(reg >= 0x0b && reg <= 0x16){
        int raw_data[6] = {0};
        for(int i = 0; i<num/2; i++){
            raw_data[starting_index + i] = (read_int[starting_index + i*2]<<8)|read_int[starting_index + i*2+1];
        }
        float actual_data[32] = {0};
        for(int i = 0; i<num/2; i++){
            if(raw_data[starting_index + i] > 0x7fff){
                if(starting_index + i<=2){
                    actual_data[starting_index + i] = -((raw_data[starting_index + i]-0x8000)*accel_parameters.fsr)/16384.0;
                }else{
                    actual_data[starting_index + i] = -((raw_data[starting_index + i]-0x8000)*gyro_parameters.fsr)/16384.0;
                }
            }else{
                if(starting_index + i<=2){
                    actual_data[starting_index + i] = (raw_data[starting_index + i]*accel_parameters.fsr)/16384.0;
                }else{
                    actual_data[starting_index + i] = (raw_data[starting_index + i]*gyro_parameters.fsr)/16384.0;
                }
            }      
        }
        for(int i = starting_index; i<6;i++){;
            readings_table[i] = actual_data[i];
        }
    }

}

void drv::read_polling_from_imu(int reg, int num, char *data_from_imu){
    while(true){    
        char drdy;
        read_from_imu(0x39,1,&drdy);
        if(drdy == 1){
            read_from_imu(reg,num,data_from_imu);
            break;
        }else{
            read_from_imu(0x39,1,&drdy);
        }
    }
}

void drv::write_to_imu(int reg, int num, char *data_src){
    // int write_packet[18] = {0};
    int write_packet[32] = {0};
    char rcv_buf;
    write_registers(reg, num, data_src, write_packet);

    send(clientSocket, &write_packet[0], 1,0); //start

    send(clientSocket, &write_packet[1], 1,0); //ADW
    recv(clientSocket, &rcv_buf,1,0);
    // printf("%d/n",rcv_buf);

    send(clientSocket, &write_packet[2], 1,0); //RA
    recv(clientSocket, &rcv_buf,1,0);

    int data_count = 0;
    while(write_packet[3+data_count] != STOP_CONDITION){
        send(clientSocket, &write_packet[3+data_count++], 1,0); //DATA
        recv(clientSocket, &rcv_buf,1,0);
    }

    send(clientSocket, &write_packet[3+data_count], 1,0); //Stop
}

bool drv::analyze_FF(float thresh){
    if(fabs(readings.ax) < thresh && fabs(readings.ay) < thresh && fabs(readings.az) < thresh){
        return true;
    }
    else{
        return false;
    }
}

int drv::get_starting_index(int reg){
    if(reg >= 11 && reg <= 22){
        int j = 0;
        for(int i = 0;i<11;i+=2){
            if(reg == 11+i || reg == 12+i){
                return j;
            }
            else{
                j++;
            }
        }
        return 0;
    }else{
        return 0;
    }
}