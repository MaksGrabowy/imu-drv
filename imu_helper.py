import csv
import matplotlib.pyplot as plt
import numpy as np
csv_rows = []
with open('2023-01-16-15-33-09-imu.csv', 'r', newline='') as csvfile:
    imu_reader = csv.reader(csvfile, delimiter=',')
    for row in imu_reader:
        csv_rows.append(row)

readings = [[],[],[],[],[],[]]
for i in range(1,len(csv_rows)):
    row = csv_rows[i]
    row_float = []
    for item in row:
        row_float.append(float(item))
    for j in range(len(row_float)):
        readings[j].append(row_float[j])

x = []
for i in range(3500):
    x.append(i)
    
plt.plot(x,readings[0],'r')
plt.plot(x,readings[1],'g')
plt.plot(x,readings[2],'b')
plt.show()